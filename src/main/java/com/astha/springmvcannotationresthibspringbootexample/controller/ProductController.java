package com.astha.springmvcannotationresthibspringbootexample.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.astha.springmvcannotationresthibspringbootexample.model.Product;
import com.astha.springmvcannotationresthibspringbootexample.service.IProductService;

@RestController
public class ProductController {

	@Autowired
	private IProductService ps;
	
	
	@GetMapping("/products")
	public ResponseEntity<List<Product>> get(){
	 return new ResponseEntity(ps.get(),HttpStatus.OK);
	}
	
	@GetMapping("/product/{id}")
	public ResponseEntity getSpecific(@PathVariable("id") int id){
		return new ResponseEntity(ps.getSpecific(id), HttpStatus.OK);
	}
	
	@PostMapping("/product")
	public ResponseEntity create(@RequestBody Product prd){
		return new ResponseEntity(ps.create(prd), HttpStatus.OK);
	}
	
	@PutMapping("/product/{id}")
	public ResponseEntity update(@PathVariable("id") int id, @RequestBody Product prd){
		return new ResponseEntity(ps.update(id, prd), HttpStatus.OK);
	}
	
	@DeleteMapping("/product/{id}")
	public ResponseEntity delete(@PathVariable("id") int id){
		return new ResponseEntity(ps.delete(id), HttpStatus.OK);
	}
}
