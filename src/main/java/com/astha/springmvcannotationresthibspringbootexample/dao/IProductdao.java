package com.astha.springmvcannotationresthibspringbootexample.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.astha.springmvcannotationresthibspringbootexample.model.Product;

@Repository
public interface IProductdao extends JpaRepository<Product, Integer> {
	Product findById(int id);
}
