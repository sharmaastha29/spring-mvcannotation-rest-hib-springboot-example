package com.astha.springmvcannotationresthibspringbootexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMvcannotationRestHibSpringbootExampleApplication {

	public static void main(String[] args) {
		System.out.println("in main");
		SpringApplication.run(SpringMvcannotationRestHibSpringbootExampleApplication.class, args);
	}

}
