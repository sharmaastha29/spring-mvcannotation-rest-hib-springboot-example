package com.astha.springmvcannotationresthibspringbootexample.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.astha.springmvcannotationresthibspringbootexample.model.Product;


public interface IProductService {
	List<Product> get();
	Product getSpecific(int id);
	Product create(Product prd);
	Product update(int id, Product prd);
	int  delete(int id);
}
