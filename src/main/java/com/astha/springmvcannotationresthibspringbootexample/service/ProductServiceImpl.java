package com.astha.springmvcannotationresthibspringbootexample.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.astha.springmvcannotationresthibspringbootexample.dao.IProductdao;
import com.astha.springmvcannotationresthibspringbootexample.model.Product;

@Service
public class ProductServiceImpl implements IProductService {
	
	@Autowired
	private IProductdao p;

	public List<Product> get(){
		return p.findAll();
	}
	
	public Product getSpecific(int id){
		return p.findById(id);
	}
	
	public Product create(Product prd){
		return p.save(prd);
	}
	
	public Product update(int id, Product prd){
		Product prod=p.findById(id);
		  prod.setName(prd.getName()); 
		  prod.setBrand(prd.getBrand());
		 return  p.save(prod);
	}
	
	public int  delete(int id){
		Product prod =p.findById(id);
		 p.delete(prod); 
		 return prod.getId();
	}
}
